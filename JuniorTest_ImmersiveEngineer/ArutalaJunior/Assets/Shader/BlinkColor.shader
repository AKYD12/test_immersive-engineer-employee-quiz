﻿Shader "Custom/BlinkColor" {
	Properties{
		_ColorA("ColorA", Color) = (1,1,1,1)
		_ColorB("ColorB", Color) = (1,1,1,1)
			_Amount("Amount", Range(0,1)) = 0
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#pragma surface surf Lambert

			fixed4 _ColorA;
			fixed4 _ColorB;
			half _Amount;

			struct Input {
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutput o) {
				_Amount += abs(sin(_Time.w));
				o.Albedo = lerp(_ColorA.rgb, _ColorB.rgb, _Amount);
			}
			ENDCG
		  }
			  FallBack "Diffuse"
}