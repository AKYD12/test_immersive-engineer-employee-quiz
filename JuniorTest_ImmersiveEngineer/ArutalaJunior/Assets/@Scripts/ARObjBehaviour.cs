﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ARObjBehaviour : MonoBehaviour, ITrackableEventHandler
{
    //Transfrom obj and particle
    public GameObject ARObj;
    public GameObject panel;
    public GameObject vfx;
    public AudioSource bgm;


    //Speed spawn, gonna effect fade shader
    public float speedSpawn;

    //Tracked flag
    //public bool tracked;

    //is obj fade in?
    [SerializeField] private bool _isFadeIn = true;
    private TrackableBehaviour _trackableBehaviour;

    private void Awake()
    {
        //Assign trackable behaviours
        _trackableBehaviour = GetComponent<TrackableBehaviour>();
        if (_trackableBehaviour)
        {
            _trackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }


    //If image tracked
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        //Check status
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
             newStatus == TrackableBehaviour.Status.TRACKED ||
             newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED
             )
        {
            //when detected
            StartCoroutine(OnDetected());
        }
        else
        {
            //when not tracked
            OnTrackingLost();
        }
    }

    private void OnTrackingLost()
    {
        //deactivate Object
        panel.SetActive(false);
        vfx.SetActive(false);
        ARObj.SetActive(false);

        //stop audio
        bgm.Stop();
    }

    IEnumerator OnDetected()
    {
        //PLay animation
        panel.SetActive(true);
        panel.GetComponent<Animator>().Play("PanelCard");

        //Wait until animation panel done
        yield return new WaitForSeconds(panel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length
            + panel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime);
        //play audio
        bgm.Play();
        //play particle
        vfx.SetActive(true);

        //Set object active
        ARObj.SetActive(true);
        ARObj.GetComponent<Animator>().Play("PopIn");
    }
}
