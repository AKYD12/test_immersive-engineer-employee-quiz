﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class InputManager : MonoBehaviour
{
    //TrackedObj
    public TrackableBehaviour trackableImg;

    //Camera
    [Header ("Zooming")]
    public Camera cam;
    public float min, max, zoomSpeed;

    //Difine speed rotation
    [Header("Rotate Swipe")]
    public float rotationSpeed;

    //Rigidbody object
    [SerializeField] private Rigidbody _rb;
    private Animator _animator;

    //GC
    private Touch _fristTouch, _secondTouch;
    private Vector3 _scaleObj;
    private float _oldTouchDistance;
    private float _currentTouchDistance;


    private void Awake()
    {
        //Get animator
        _animator = _rb.GetComponent<Animator>();
    }


    public void QuitApp()
    {
        Application.Quit();
    }

    private void FixedUpdate()
    {
        if (trackableImg.CurrentStatus == TrackableBehaviour.Status.DETECTED
            || trackableImg.CurrentStatus == TrackableBehaviour.Status.EXTENDED_TRACKED
            || trackableImg.CurrentStatus == TrackableBehaviour.Status.TRACKED)
        {
#if UNITY_EDITOR
            //Test in editor
            if (Input.GetMouseButton(0))
            {
                //Disable animator so we can zoom or interact
                _animator.enabled = false;

                //Define x and y input from mouse
                //Get swipe
                float x = Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
                float y = Input.GetAxis("Mouse Y") * rotationSpeed * Time.deltaTime;

                //apply torque
                _rb.AddTorque(Vector3.down * x);
                _rb.AddTorque(Vector3.right * y);
            }
            else
            {
                //enabling animator animator resume play
                _animator.enabled = true;
            }
#endif

#if UNITY_ANDROID
            //Swipe
            //check if there was an input
            if (Input.touchCount > 0 && Input.touchCount < 2)
            {
                _fristTouch = Input.GetTouch(0);
                //swipe
                if (_fristTouch.phase == TouchPhase.Moved)
                {
                    //Disable animator so we can zoom or interact
                    _animator.enabled = false;

                    //Define X and Y Swipe
                    float x = _fristTouch.deltaPosition.x * rotationSpeed * Time.deltaTime;
                    float y = _fristTouch.deltaPosition.y * rotationSpeed * Time.deltaTime;


                    //Apply swipe torque
                    _rb.AddTorque(Vector3.down * x);
                    _rb.AddTorque(Vector3.right * y);
                }

            }
            else
            {
                //enabling animator animator resume play
                _animator.enabled = true;
            }

            //Zooming
            //Check if there was two inputs
            if (Input.touchCount >= 2)
            {
                //Disable animator so we can zoom or interact
                _animator.enabled = false;

                //input one and two
                _fristTouch = Input.GetTouch(0);
                _secondTouch = Input.GetTouch(1);

                //Get Touch Position
                if (_fristTouch.phase == TouchPhase.Began || _secondTouch.phase == TouchPhase.Began)
                {
                    _scaleObj = _rb.transform.localScale; 
                    _oldTouchDistance = Vector3.Distance(_fristTouch.position, _secondTouch.position);
                }

                //Compare intial touch pos with current pos
                else if (_fristTouch.phase == TouchPhase.Moved || _secondTouch.phase == TouchPhase.Moved)
                {
                    _currentTouchDistance = Vector2.Distance(_fristTouch.position, _secondTouch.position);
                    float scaleFactor = _currentTouchDistance / _oldTouchDistance;

                    //scale obj
                    _rb.transform.localScale = _scaleObj * scaleFactor;
                }
            }
#endif
        }
    }
}
