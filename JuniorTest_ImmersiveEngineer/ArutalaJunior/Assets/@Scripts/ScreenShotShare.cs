﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
public class ScreenShotShare : MonoBehaviour
{
    //Test Case
    //public Text pathDir, pathSc;

    [Header("Camera")]
    public Camera cam;

    [Header("Resolution")]
    public int width;
    public int height;

    //where screenshot gonna saved
    string _directory;

    //Prevent double process
    bool _isProcessing = false, _appFocus = true;

    private void Start()
    {
        //Check directory and create if null
        _directory = "/storage/emulated/0/DCIM/ScreenShotArutala"; 
        if (!Directory.Exists(_directory))
        {
            Directory.CreateDirectory(_directory);
        }
    }

    public void TakeScreenShot()
    {
        StartCoroutine(ScreenShootAndShare());
    }

    IEnumerator ScreenShootAndShare()
    {
        //Prevent double process
        _isProcessing = true;
        yield return new WaitForEndOfFrame();

        //create render texture
        RenderTexture rt = new RenderTexture(width, height, 24);
        cam.targetTexture = rt;

        //Create texture 2d and render texture
        Texture2D screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
        cam.Render();

        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        cam.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        Destroy(rt);

        //Save byte texture
        byte[] bytes = screenShot.EncodeToPNG();
        //Name file
        string filename = "Screenshot" + DateTime.Now.ToString("dd-MM-yyy-HH-mm-ss") + ".png";
        //Destination save file
        string destination = Path.Combine(_directory, filename);

        //Test case
        //pathDir.text = destination;

        //Save to gallery DCIM
        System.IO.File.WriteAllBytes(destination, bytes);

        yield return new WaitForSecondsRealtime(0.3f);

        //Android Content
        if (!Application.isEditor)
        {
            AndroidJavaClass _intent = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject _object = new AndroidJavaObject("android.content.Intent");

            _object.Call<AndroidJavaObject>("setAction", _intent.GetStatic<string>("ACTION_SEND"));
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
            //pathSc.text = "file://" + destination;
            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);

            //Method call for sharing object/data=======================
            //use putExtra method from android.content.intent to add data
            _object.Call<AndroidJavaObject>("putExtra", _intent.GetStatic<string>("EXTRA_STREAM"),
                uriObject);

            //tell if we want to share imagetype
            _object.Call<AndroidJavaObject>("setType", "image/jpeg");
            //=========================================================

            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject popUpShare = _intent.CallStatic<AndroidJavaObject>("createChooser",
                _object, "Share screenshot");
            currentActivity.Call("startActivity", popUpShare);

            yield return new WaitForSecondsRealtime(1);
        }

        _isProcessing = false;
    }

}
