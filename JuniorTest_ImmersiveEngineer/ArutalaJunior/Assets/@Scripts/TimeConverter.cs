﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeConverter : MonoBehaviour
{
    public InputField inputField;
    public Text output;

   [SerializeField] int _hour, _min, _sec;

    public void CorrectInput()
    {
        string time = ConvertAndCorrect();

        //Correct Input
        inputField.text = time;
    }


    [ContextMenu("TRY SPLIT")]
    public void Convert()
    {

        string time = ConvertAndCorrect();
        //Output
        output.text =  DateTime.Parse(time).ToString("HH:mm:ss");

    }


    private string ConvertAndCorrect()
    {
        //Get char from range to define time
        string time = inputField.text.Substring(0, 8);

        //Get char start from previous index
        string amPm = inputField.text.Substring(time.Length);

        //Spit so we can define the value of hour min and sec
        string[] split = new string[3];
        //Parsing string split
        split = time.Split(char.Parse(":"));

        //Define time
        _hour = int.Parse(split[0]);
        _min = int.Parse(split[1]);
        _sec = int.Parse(split[2]);

        //Constrain hour
        if (_hour >= 12)
        {
            _hour = 12;
        }
        if (_hour < 0)
        {
            _hour = 0;
        }
        //output = auto correct 15:00:00 => 12:00:00

        //Constrain min
        if (_min <= 0)
        {
            _min = 0;
        }
        if (_min > 59)
        {
            _min = 59;
        }
        //output = auto correct 01:90:00 => 01:59:00


        //Constrain sec
        if ( _sec <= 0)
        {
            _sec = 0;
        } if (_sec > 59)
        {
            _sec = 69;
        }
        //output = auto correct 01:00:99 => 01:00:59

        //arrange string
        time = string.Format("{0}:{1}:{2}{3}", _hour.ToString("00"), _min.ToString("00"), _sec.ToString("00"), amPm);
        return time;
    }
}